# denite.nvim for Pearl

🐉 Dark powered asynchronous unite all interfaces for Neovim/Vim8

## Details

- Plugin: https://github.com/Shougo/denite.nvim
- Pearl: https://github.com/pearl-core/pearl
